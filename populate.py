from app import app,db
from datetime import datetime
from app.models import *

# add team
team=Team(id=1,teamName='Admin')
db.session.add(team)
team=Team(id=2,teamName='User')
db.session.add(team)


# add rooms
room=Room(id=0,roomName='Admin Room')
db.session.add(room)
room=Room(id=1,roomName='127 Marconi')
db.session.add(room)
room=Room(id=2,roomName='339b Marconi')
db.session.add(room)
room=Room(id=3,roomName='117-118 Fermi')
db.session.add(room)
room=Room(id=4,roomName='314 Fermi')
db.session.add(room)
room=Room(id=5,roomName='413 Fermi')
db.session.add(room)


# add admin & users
user=User(id=1,username='admin',fullname='admin',position='admin',teamId=1, roomId=0)
user.set_password('admin')
db.session.add(user)

user=User(id=2,username='alice',fullname='Alice',position='Physics PhD 33',teamId=2, roomId=1)
user.set_password('alice')
db.session.add(user)


db.session.commit()
