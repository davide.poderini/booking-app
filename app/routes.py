from flask import render_template, flash, redirect, request, url_for
from werkzeug.urls import url_parse
from app import app
from app.forms import *
from flask_login import current_user, login_user, logout_user, login_required
from app.models import *
from app import db
import sqlite3

partthreshold=2

#OK
@app.route('/')
@app.route('/index')
def index():
    return render_template('index.html')

#OK
@app.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        if user is None or not user.check_password(form.password.data):
            flash('Invalid username or password')
            return redirect(url_for('login'))
        login_user(user, remember=form.remember_me.data)
        next_page=request.args.get('next')
        if not next_page or url_parse(next_page).netloc!='':
            next_page=url_for('index')
        return redirect(next_page)
    return render_template('login.html', title='Sign In', form=form)

#OK
@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('index'))

#OK
@app.route('/register', methods=['GET','POST'])
def register():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form =RegistrationForm()
    if form.validate_on_submit():
        user=User(username=form.username.data,\
                  fullname=form.fullname.data,\
                  position=form.position.data,\
                  roomId=(Room.query.filter_by(id=form.room.data).first()).id,\
                  teamId=2)   #SF assuming only Users group here (others to be added by admin)
        user.set_password(form.password.data)
        db.session.add(user)
        team=Team.query.filter_by(id=user.teamId).first()
        if team is None:
            newTeam=Team(id=user.teamId,\
                         teamName=form.teamName.data)
            db.session.add(newTeam)
            db.session.commit()
            flash('Registered with a new team created')
            return redirect(url_for('login'))
        else:
            db.session.commit()
            flash('Registered to an existing team')
            return redirect(url_for('login'))
    return render_template('register.html',title='Register',form=form)

#OK
@app.route('/changepwd', methods=['GET','POST'])
def changepwd():
    if not current_user.is_authenticated:
        flash('Please Log in before changing password')
        return redirect(url_for('login'))
    form = ChangePwdForm()
    if form.validate_on_submit():
        if current_user.check_password(form.opassword.data):
            current_user.set_password(form.password.data)
            db.session.commit()
            flash('Password changed successfully!')
            return redirect(url_for('index'))
        else:
            flash('Old password does not match')
            return redirect(url_for('changepwd'))
    return render_template('changepwd.html', title='Change password',form=form)

#OK
@app.route('/adduser', methods=['GET','POST'])
@login_required
def adduser():
    if not current_user.is_authenticated:
        flash('Please Log in as admin to add user')
        return redirect(url_for('login')) 
    if current_user.username!='admin':
        flash('Please Log in as admin to add user')
        return redirect(url_for('login'))
    form =AdduserForm()
    if form.validate_on_submit():
        user=User(username=form.username.data,\
                  fullname=form.fullname.data,\
                  position=form.position.data,\
                  teamId=form.teamId.data,\
                  roomId=(Room.query.filter_by(id=form.room.data).first()).id)
        user.set_password(form.password.data)
        db.session.add(user)
        team=Team.query.filter_by(id=user.teamId).first()
        if team is None:
            newTeam=Team(id=user.teamId,\
                         teamName=form.teamName.data)
            db.session.add(newTeam)
            db.session.commit()
            flash(f'Added user {form.username.data} with a new team created')
            return redirect(url_for('adduser'))
        else:
            db.session.commit()
            flash(f'Added user {form.username.data} to an existing team')
            return redirect(url_for('adduser'))
    return render_template('adduser.html',title='Add User',form=form)

#OK
@app.route('/addteam',methods=['GET','POST'])
@login_required
def addteam():
    if not current_user.is_authenticated:
        flash('Please Log in as admin to add team')
        return redirect(url_for('login')) 
    if current_user.username!='admin':
        flash('Please Log in as admin to add team')
        return redirect(url_for('index'))
    form=AddteamForm()
    if form.validate_on_submit():
        team=Team(id=form.id.data,\
                  teamName=form.teamName.data)
        db.session.add(team)
        db.session.commit()
        flash(f'Team {form.teamName.data} successfully added!')
        return redirect(url_for('addteam'))
    return render_template('addteam.html',title='Add Team',form=form)

#OK
@app.route('/deleteteam',methods=['GET','POST'])
@login_required
def deleteteam():
    if not current_user.is_authenticated:
        flash('Please Log in as admin to delete team')
        return redirect(url_for('login')) 
    if current_user.username!='admin':
        flash('Please Log in as admin to delete team')
        return redirect(url_for('index'))
    form=DeleteteamForm()
    
    if form.validate_on_submit():
        team=Team.query.filter_by(id=form.ids.data).first()

        bookings=Booking.query.filter_by(teamId=team.id).all()
        hasFutureBooking=False
        for booking in bookings:
            if booking.date>datetime.now():
                hasFutureBooking=True
                break
        if hasFutureBooking:
            flash('You cannot delete a team that holds future bookings!')
            return redirect(url_for('deleteteam'))

        # delete all users in a deleted team
        userInTeam=User.query.filter_by(teamId=form.ids.data).all()
        for user in userInTeam:
            db.session.delete(user)
        db.session.delete(team)
        db.session.commit()
        flash(f'Team {team.teamName} and team members successfully deleted! Please register member again to other team')
        return redirect(url_for('index'))
    form=DeleteteamForm()
    return render_template('deleteteam.html',title='Delete Team',form=form)

#OK
@app.route('/deleteuser',methods=['GET','POST'])
@login_required
def deleteuser():
    if not current_user.is_authenticated:
        flash('Please Log in as admin to delete user')
        return redirect(url_for('login')) 
    if current_user.username!='admin':
        flash('Please Log in as admin to delete user')
        return redirect(url_for('index'))
    
    form=DeleteuserForm()
    if form.validate_on_submit():
        user=User.query.filter_by(id=form.ids.data).first()

        bookings=Booking.query.filter_by(bookerId=user.id).all()
        hasFutureBooking=False
        for booking in bookings:
            if booking.date>datetime.now():
                hasFutureBooking=True
                break
        if hasFutureBooking:
            flash('You cannot delete a user that holds future bookings!')
            return redirect(url_for('deleteuser'))

        db.session.delete(user)
        db.session.commit()
        flash(f'User {user.username} successfully deleted! ')
        return redirect(url_for('index'))
    return render_template('deleteuser.html',title='Delete User',form=form)

#OK
@app.route('/book',methods=['GET','POST'])
@login_required
def book():
    if not current_user.is_authenticated:
        flash('Please Log in before booking')
        return redirect(url_for('login'))
    form=BookroomForm()
    if form.validate_on_submit():
        # make booking
        booker=current_user
        team=Team.query.filter_by(id=current_user.teamId).first()
        room=Room.query.filter_by(id=current_user.roomId).first()
        
        # check time collision
        bookingcollisions=Booking.query.filter_by(date=datetime.combine(form.date.data,datetime.min.time())).filter_by(roomId=room.id).all()

        countcollisions=0
        
        for bookingcollision in bookingcollisions:
            # [a, b] overlaps with [x, y] if b > x and a < y
            if (form.startTime.data<bookingcollision.endTime and form.endTime.data>bookingcollision.startTime):
                flash(f'The time from {bookingcollision.startTime} to {bookingcollision.endTime} is already booked by {User.query.filter_by(id=bookingcollision.bookerId).first().fullname}.')
                countcollisions+=1

        if countcollisions>partthreshold:
            return redirect(url_for('book'))


        booking=Booking(teamId=team.id,roomId=room.id,bookerId=booker.id,date=form.date.data,startTime=form.startTime.data,endTime=form.endTime.data)
        db.session.add(booking)

        db.session.commit()
        flash('Booking success!')
        return redirect(url_for('index'))
    return render_template('book.html',title='Book room',form=form)

#OK
@app.route('/cancelbooking',methods=['GET','POST'])
@login_required
def cancelbooking():
    if not current_user.is_authenticated:
        flash('Please Log in to cancel booking')
        return redirect(url_for('login')) 
    
    form=CancelbookingForm()
    if form.validate_on_submit():
        booking=Booking.query.filter_by(id=form.ids.data).first()

        if booking.date<=datetime.now():
            flash(f'Past booking cannot be canceled')
            return redirect(url_for('cancelbooking'))
        
        db.session.delete(booking)
        db.session.commit()
        flash(f'Booking successfully deleted! ')
        return redirect(url_for('index'))
    return render_template('cancelbooking.html',title='Cancel Booking',form=form)

#OK
@app.route('/roomoccupation',methods=['GET','POST'])
def roomoccupation():
    form=RoomoccupationForm()
    if form.validate_on_submit():
        roomoccus=[]
        hours=range(8,19)
        rooms=Room.query.all()
        allrooms=[]
        for room in rooms:
            if room.roomName=="Admin Room": continue                
            roomoccu=dict()
            roomoccu['roomName']=room.roomName
            roomoccu['roomhours']=[0]*len(hours)
            #bad loop - rephrase asap
            for hour in hours:
                bookings=Booking.query.filter_by(date=datetime.combine(form.date.data,datetime.min.time())).filter_by(roomId=room.id).all()
                
                for booking in bookings:
                    if (hour+0.5)<booking.endTime and (hour+0.5)>booking.startTime:
                        roomoccu['roomhours'][hour-8]+=1
                        
            roomoccus.append(roomoccu)
            allrooms.append({'roomName':room.roomName})
            
        return render_template('roomoccupationlist.html',title='Room Occupation',roomoccus=roomoccus,date=form.date.data,hours=[str(hour) for hour in hours],allrooms=allrooms, partthreshold=partthreshold)
    return render_template('roomoccupation.html',title='Room Occupation Status',form=form)

#OK
@app.route('/bookinghistory')
def bookinghistory():
    bookings=Booking.query.order_by(Booking.date).all()
    bookingreturns=[]
    for booking in bookings:
        bookingreturn=dict()
        bookingreturn['room']=Room.query.filter_by(id=booking.roomId).first().roomName
        bookingreturn['booker']=User.query.filter_by(id=booking.bookerId).first().fullname
        bookingreturn['date']=booking.date.date()
        bookingreturn['time']=f'{booking.startTime} to {booking.endTime}'
        bookingreturns.append(bookingreturn)
    return render_template('bookinghistory.html',title='Booking History', bookings=bookingreturns)

#OK
@app.route('/bookingparticipants',methods=['GET','POST'])
def bookingparticipants():
    form=BookingparticipantsForm()
    if form.validate_on_submit():
        bookings=Booking.query.filter_by(date=datetime.combine(form.date.data,datetime.min.time())).filter_by(roomId=form.room.data).all()
        participants=[]
        for booking in bookings:
            participants.append(f'{User.query.filter_by(id=booking.bookerId).first().fullname}')
        return render_template('bookingparticipantslist.html',title='Booking Participants',room=Room.query.filter_by(id=form.room.data).first().roomName, date=form.date.data, participants=participants)
    return render_template('bookingparticipants.html',title='Booking Participants',form=form)
