from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField, SubmitField, IntegerField, SelectField, DateField, SelectMultipleField, widgets
from wtforms.validators import ValidationError, DataRequired, Email, EqualTo
from flask_login import current_user
from app.models import *
import datetime


#OK
class LoginForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])
    remember_me = BooleanField('Remember Me')
    submit = SubmitField('Sign In')

#OK
class RoomChoiceIterable(object):
    def __iter__(self):
        rooms=Room.query.all()
        choices=[(room.id,room.roomName) for room in rooms] 
        for choice in choices:
            if choice[0]!=0:
                yield choice

#OK
class RegistrationForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])
    password2 = PasswordField('Confirm password', validators=[DataRequired(), EqualTo('password')])
    fullname=StringField('Full Name',validators=[DataRequired()])
    position=StringField('Position (e.g. Physics PhD 33)',validators=[DataRequired()])
    room=SelectField('Room',coerce=int,choices=RoomChoiceIterable())
    submit=SubmitField('Register')

    def validate_username(self,username):
        user=User.query.filter_by(username=self.username.data).first()
        if user is not None: # username exist
            raise ValidationError('Please use a different username.')

#OK
class ChangePwdForm(FlaskForm):
    opassword = PasswordField('Old password', validators=[DataRequired()])
    password = PasswordField('New password', validators=[DataRequired()])
    password2 = PasswordField('Confirm password', validators=[DataRequired(), EqualTo('password')])
    submit=SubmitField('Change')

#OK
class AddteamForm(FlaskForm):
    id=IntegerField('Team number',validators=[DataRequired()])
    teamName=StringField('Team name',validators=[DataRequired()])
    submit=SubmitField('Add')

    def validate_id(self,id):
        team=Team.query.filter_by(id=id.data).first()
        if team is not None:
            raise ValidationError('Team Exist, try again')
    
    def validate_teamName(self,teamName):
        team=Team.query.filter_by(teamName=teamName.data).first()
        if team is not None:
            raise ValidationError('Team Name Exist, try again') 

#OK
class AdduserForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])
    fullname=StringField('Full Name',validators=[DataRequired()])
    position=StringField('Position',validators=[DataRequired()])
    teamId=IntegerField('Team number',validators=[DataRequired()])
    teamName=StringField('Team name',validators=[DataRequired()])
    room=SelectField('Room',coerce=int,choices=RoomChoiceIterable())
    submit=SubmitField('Register')

    def validate_username(self,username):
        user=User.query.filter_by(username=self.username.data).first()
        if user is not None: # username exist
            raise ValidationError('Please use a different username.')
    
    def validate_teamId(self,teamId):
        team=Team.query.filter_by(id=teamId.data).first()
        if team is not None:
            if team.teamName!=self.teamName.data:
                raise ValidationError('Team name does not match, try again.')

#OK
class TeamChoiceIterable(object):
    def __iter__(self):
        teams=Team.query.all()
        choices=[(team.id,team.teamName) for team in teams] 
        choices=[choice for choice in choices if choice[1]!='Admin']
        for choice in choices:
            yield choice

#OK
class DeleteteamForm(FlaskForm):
    ids=SelectField('Choose Team',choices=TeamChoiceIterable(),coerce=int)
    submit=SubmitField('Delete')

#OK
class UserChoiceIterable(object):
    def __iter__(self):
        users=User.query.all()
        choices=[(user.id,f'{user.fullname}, team {Team.query.filter_by(id=user.teamId).first().teamName}') for user in users] 
        choices=[choice for choice in choices if 'admin' not in choice[1]] # do not delete admin
        for choice in choices:
            yield choice

#OK
class DeleteuserForm(FlaskForm):
    ids=SelectField('Choose User',coerce=int,choices=UserChoiceIterable())
    submit=SubmitField('Delete')

#OK
class BookroomForm(FlaskForm):
    date=DateField('Choose date', format="%m/%d/%Y",validators=[DataRequired()])
    startTime=SelectField('Choose starting time (in 24hr expression)',coerce=int,choices=[(i,i) for i in range(8,19)])
    endTime=SelectField('Choose end time (in 24hr expression)',coerce=int,choices=[(i,i) for i in range(8,19)])
    submit=SubmitField('Book')

    def validate_date(self,date):
        if self.date.data<datetime.datetime.now().date() or self.date.data>datetime.datetime.now().date()+datetime.timedelta(days=3):
            raise ValidationError('You can only book for day>=today and day<today+3')

    def validate_endTime(self,endTime):
        if endTime.data<self.startTime.data:
            raise ValidationError('End Time must be after Start Time')
    
#OK
class BookingChoiceIterable(object):
    def __iter__(self):
        bookings=Booking.query.filter_by(bookerId=current_user.id).all()
        choices=[(booking.id,f'{User.query.filter_by(id=booking.bookerId).first().fullname} in {Room.query.filter_by(id=booking.roomId).first().roomName} start {booking.date.date()} from {booking.startTime}') for booking in bookings] 
        for choice in choices:
            yield choice

#OK
class CancelbookingForm(FlaskForm):
    ids=SelectField('Choose booking to cancel',coerce=int,choices=BookingChoiceIterable()) 
    submit=SubmitField('Cancel') 

#OK
class RoomoccupationForm(FlaskForm):
    date=DateField('Choose date', format="%m/%d/%Y",validators=[DataRequired()])
    submit=SubmitField('Check')

#OK
class BookingparticipantsForm(FlaskForm):
    room=SelectField('Room',coerce=int,choices=RoomChoiceIterable())
    date=DateField('Choose date', format="%m/%d/%Y",validators=[DataRequired()])
    submit=SubmitField('Check')  
